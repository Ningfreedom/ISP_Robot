*** setting ***
Library             Selenium2Library


*** Variables ***
${URL}                  https://www.set.or.th/investnowstarter/#/
${FIRSTPAGE}            Starter
${BROWSER}              chrome
${INPUTAGE}             คุณจะทำงานเก็บเงินถึงอายุเท่าไหร่
${CurrentAge}           60
${RetireAge}            60
${DieAge}               80
${workingPeriod}        0
${retirePeriod}         20
${expensePerMonth}      15000
${currentSaving}        3000000
#${savingPerMonth}       40000
${badresult}            แย่แล้ว
${totalSavingBefore}    4,399,751.95
${totalSavingAfter}     3,000,000.00



*** Test Case ***
Investment saving plan for enough money
    Open Welcome page and Click next
    Open Input Age
    Test Button for return to first page and back to current page
    Input expense current and saving plan per month
#    Show The Result
    Back to last page and back to current page and close browser

Do survey question
    Do survey question








*** Keywords ***
Open Welcome page and Click next
    Open Browser            ${URL}      ${BROWSER}
    Page Should Contain     ${FIRSTPAGE}
    Click Element           id = clickBtn
Open Input Age
    Page Should Contain     ${INPUTAGE}
    Input Text              id = currentAge         ${CurrentAge}
    Input Text              id = retireAge          ${RetireAge}
    Input Text              id = dieAge             ${DieAge}
    Element Should Contain          id = workingPeriod      ${workingPeriod}
    Element Should Contain          id = retirePeriod       ${retirePeriod}
    Click Element           id = nextBtn

Test Button for return to first page and back to current page
    Click Element           id = backBtn
    Click Element           id = nextBtn

Input expense current and saving plan per month
    Page Should Contain     แผนเงินเก็บของคุณ
    Input Text              id = expensePerMonth        ${expensePerMonth}
    Input Text              id = currentSaving          ${currentSaving}
    #Input Text              id = savingPerMonth         ${savingPerMonth}
    Click Element           id = nextBtn
#Show The Result
    #Page Should Contain         ${badresult}
    #Page Should Contain         ${totalSavingBefore}
    #Page Should Contain         ${totalSavingAfter}
Back to last page and back to current page and close browser
    Click Element               id = backBtn
    Click Element               id = nextBtn
   
Do survey question
    Click Element               id = helpBtn
    Page should Contain         10,112
    Page should Contain         14
    Click Element               id = backBtn
    Close Browser
